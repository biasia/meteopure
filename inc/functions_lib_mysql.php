
<?php
///=================  funzione di log==================
function logAll ($cf,$query,$numRows,$numFields,$linkLocale){
	$pagina=$_SERVER['PHP_SELF'];
	$ip=$_SERVER['REMOTE_ADDR'];
	$data=date('Y-m-d H:i:s');
	$query2="insert into sito.queries values(\"".$cf."\",\"".$query."\",\"".$data."\")";
	$result2= mysql_query($query2,$linkLocale);
//----- aggiorna il numero di campi scaricati dall'utente
	$query="update  sito.utenti set scaricati=scaricati+".$numRows*$numFields." where cf='".$cf."';";
	//echo $query;
	$result = mysql_query($query,$linkLocale);
	$query2="insert into sito.pages values(\"".$ip."\",\"".$data."\",\"".$cf."\",\"".$pagina."\")";
	$result2= mysql_query($query2,$linkLocale);
}


///=================  funzione di log  delle pagine==================
function logPage ($cf,$linkLocale){
	$pagina=$_SERVER['PHP_SELF'];
	$ip=$_SERVER['REMOTE_ADDR'];
	$data=date('Y-m-d H:i:s');
//	$query2="insert into sito.queries values(\"".$cf."\",\"".$query."\",\"".$data."\")";
//	$result2= mysql_query($query2,$linkLocale);
//----- aggiorna il numero di campi scaricati dall'utente
//	$query="update  sito.utenti set scaricati=scaricati+".$numRows*$numFields." where cf='".$cf."';";
	//echo $query;
//	$result = mysql_query($query,$linkLocale);
	$query2="insert into sito.pages values(\"".$ip."\",\"".$data."\",\"".$cf."\",\"".$pagina."\")";
	$result2= mysql_query($query2,$linkLocale);
}



//=============  recupero dei dati istantanei======================================================================
function getLastData($link){
// ecco la query .....
$query="select stazione as id,nome,data,ifnull(temp,NULL) as temp,
    ifnull(umid,NULL) as umid,
    ifnull(vent,NULL) as vent,
    ifnull(t50bag,NULL) as t50bag,
    ifnull(fb_ora,NULL) as fb,
    piog_oggi from meteo.ultimo_record,gestione.stazioni_4326 where
    id=stazione and (stazione>=7 and stazione<=99) order by stazione ";
$query="select nome,stazione as id,ifnull(temp,NULL) as temp,
    ifnull(umid,NULL) as umid,
    ifnull(vent,NULL) as vent,
    piog_oggi from meteo.ultimo_record,gestione.stazioni_4326 where
    id=stazione and (stazione>=7 and stazione<=99) order by nome ";


//-------------------------------------------------------------------------------
// ora che ho costruito la query "adatta" ... prelevo i dati dal DB e creo il json
//-------------------------------------------------------------------------------
//echo $query;
$result = mysql_query($query,$link)or die('Query failed: ' . mysql_error());
$rows=array();
$c=0;
while($r=mysql_fetch_assoc($result)){
    //$idx=$r['id'];
    //if (!in_array($idx, $elenco_id)) {
	//$r['fb']="null";
	//$r['t50bag']="null";
    //} else {
	//print "falso <br/>";
    //}
    $rows[$c]=$r;
    $c++;
}
//$output = json_encode($rows);
//echo $output;
return $rows;
}

//================  dati PAN========================================================
function getPanData($link){
//
// in ogni caso aggiungo le stazioni PAN di cui l'utente puo' vedere tutti i parametri
// per un periodo di max 10gg indietro
$elenco_id[]=27;
$elenco_id[]=81;
$elenco_id[]=29;
$elenco_id[]=30;
$elenco_id[]=31;
$elenco_id[]=32;

$tutto=array();

// intervallo di dati ....
$ngiorni=10;
$nore=$ngiorni*24;
$nore=1;

//print "id=$id <br/>";
// verifico se l'ID richiesto nel GET fa parte di una di queste ...
//if (in_array($id, $elenco_id)) {
    //print "trovato id $id... <br/>";
    // ecco la query .....
    //foreach($elenco_id as $id){
    // query per dati ORARI
$i=0;    
foreach($elenco_id as $id){
    $query="select $id as id,nome,data,
	ifnull(s3m52,NULL) as temp,
	round(ifnull(s4m96,NULL),0) as umid,
	ifnull(s1m50,ifnull(s13m50,NULL)) as vent,
	ifnull(s37m52,NULL) as t50bag,
	ifnull(s20m58,NULL) as fb,
	s17m45 as pioggia
	from meteonesa.st$id,gestione.stazioni_4326 where gestione.stazioni_4326.id=$id and 
	data>subdate(now(),interval $nore hour) order by data";
//echo $query;
    // query per dati GIORNALIERI
    $queryg="select $id as id,nome,data,
	ifnull(s3m52,NULL) as temp,
	ifnull(s3m781,NULL) as temp_min,
	ifnull(s3m782,NULL) as temp_max,
	ifnull(s3m52,NULL) as temp,
	round(ifnull(s4m96,NULL),0) as umid,
	ifnull(s1m50,ifnull(s13m50,NULL)) as vent,
	ifnull(s1m64,ifnull(s13m64,NULL)) as raffica,
	ifnull(s37m52,NULL) as t50bag,
	ifnull(s20m58,NULL) as fb,
	s17m45 as pioggia
	from meteonesa.stg$id,gestione.stazioni_4326 where gestione.stazioni_4326.id=$id and 
	data>subdate(now(),interval $ngiorni day) order by data";
    //id=sselect $id as id,data,s3m52 as temp,s4m96 as umid from meteonesa.st27 where data>subdate(now(),interval 1 hour) order by data";
    //$queryg="select $id as id,data,s3m52 as temp,s4m96 as umid from meteonesa.stg27 where data>subdate(now(),interval 1 day) order by data";

    //-------------------------------------------------------------------------------
    // ora che ho costruito la query "adatta" ... prelevo i dati dal DB e creo il json
    //-------------------------------------------------------------------------------

    //print "$query <br/>";
    $result = mysql_query($query,$link) or die('Query failed: ' . mysql_error());
    $resultg = mysql_query($queryg,$link) or die('Query failed: ' . mysql_error());
    $orari=array();
    while($r=mysql_fetch_assoc($result)){
	$nome=$r['nome'];
	$orari['data'][]=$r['data'];
	$orari['temp'][]=$r['temp'];
	$orari['umid'][]=$r['umid'];
	$orari['vent'][]=$r['vent'];
	$orari['t50bag'][]=$r['t50bag'];
	$orari['fb'][]=$r['fb'];
	$orari['pioggia'][]=$r['pioggia'];
    }
    $giornalieri=array();
    while($rg=mysql_fetch_assoc($resultg)){
	//$nome=$r['nome'];
	$giornalieri['data'][]=$rg['data'];
	$giornalieri['temp'][]=$rg['temp'];
	$giornalieri['temp_min'][]=$rg['temp_min'];
	$giornalieri['temp_max'][]=$rg['temp_max'];
	$giornalieri['umid'][]=$rg['umid'];
	$giornalieri['vent'][]=$rg['vent'];
	$giornalieri['raffica'][]=$rg['raffica'];
	$giornalieri['t50bag'][]=$rg['t50bag'];
	$giornalieri['fb'][]=$rg['fb'];
	$giornalieri['pioggia'][]=$rg['pioggia'];
    }
    $tutto[$i]['id']=$id;
    $tutto[$i]['nome']=$nome;
    $tutto[$i]['orari']=$orari;
    $tutto[$i]['giornalieri']=$giornalieri;
    $i++;
} 
//----------------------------------------------------
//======= ORA SCRIVO il JSON =========================
//----------------------------------------------------
print "ggvggggg<hr>";
print_r ($tutto[0]['orari']);
print $tutto['0']['orari']['data'][0];
print "ggvggggg<hr>";
$output = json_encode($tutto);
return $output;
}

// //================funzione per recuperare tutti i dati di una stazione , sia orari che giornalieri in una volta  NB verificare se ci sono limiti negli array.===================================================================
function getAllData($id,$link){
//
// intervallo di dati ....
	$ngiorni=730;
	$nore=$ngiorni*24;	
    $query="select $id as id,nome,data,
	ifnull(s3m52,NULL) as temp,
	round(ifnull(s4m96,NULL),0) as umid,
	ifnull(s1m50,ifnull(s13m50,NULL)) as vent,
	ifnull(s37m52,NULL) as t50bag,
	ifnull(s20m58,NULL) as fb,
	s17m45 as pioggia
	from meteonesa.st$id,gestione.stazioni_4326 where gestione.stazioni_4326.id=$id and 
	data>subdate(now(),interval $nore hour) order by data";
    // query per dati GIORNALIERI
    $queryg="select $id as id,nome,data,
	ifnull(s3m52,NULL) as temp,
	ifnull(s3m781,NULL) as temp_min,
	ifnull(s3m782,NULL) as temp_max,
	ifnull(s3m52,NULL) as temp,
	round(ifnull(s4m96,NULL),0) as umid,
	ifnull(s1m50,ifnull(s13m50,NULL)) as vent,
	ifnull(s1m64,ifnull(s13m64,NULL)) as raffica,
	ifnull(s37m52,NULL) as t50bag,
	ifnull(s20m58,NULL) as fb,
	s17m45 as pioggia
	from meteonesa.stg$id,gestione.stazioni_4326 where gestione.stazioni_4326.id=$id and 
	data>subdate(now(),interval $ngiorni day) order by data";
    
    //-------------------------------------------------------------------------------
    // ora che ho costruito la query "adatta" ... prelevo i dati dal DB e creo il json
    //-------------------------------------------------------------------------------

    //print "$query <br/>";
    $result = mysql_query($query,$link) or die('Query failed: ' . mysql_error());
    $resultg = mysql_query($queryg,$link) or die('Query failed: ' . mysql_error());
    $orari=array();
    while($r=mysql_fetch_assoc($result)){
	$nome=$r['nome'];
	$orari['data'][]=$r['data'];
	$orari['temp'][]=$r['temp'];
	$orari['umid'][]=$r['umid'];
	$orari['vent'][]=$r['vent'];
	$orari['t50bag'][]=$r['t50bag'];
	$orari['fb'][]=$r['fb'];
	$orari['pioggia'][]=$r['pioggia'];
}
    $giornalieri=array();
    while($rg=mysql_fetch_assoc($resultg)){
	//$nome=$r['nome'];
	$giornalieri['data'][]=$rg['data'];
	$giornalieri['temp'][]=$rg['temp'];
	$giornalieri['temp_min'][]=$rg['temp_min'];
	$giornalieri['temp_max'][]=$rg['temp_max'];
	$giornalieri['umid'][]=$rg['umid'];
	$giornalieri['vent'][]=$rg['vent'];
	$giornalieri['raffica'][]=$rg['raffica'];
	$giornalieri['t50bag'][]=$rg['t50bag'];
	$giornalieri['fb'][]=$rg['fb'];
	$giornalieri['pioggia'][]=$rg['pioggia'];
    } 
//----------------------------------------------------
//======= ORA SCRIVO il JSON =========================
//----------------------------------------------------
$tutto=array();
$tutto['id']=$id;
$tutto['nome']=$nome;
$tutto['orari']=$orari;
$tutto['giornalieri']=$giornalieri;
$output = json_encode($tutto);
return $output;
}

//================  crea i menu per le ricerche dati e i grafici===================================================================
function creaMenu($stazioni,$linkLocale,$scansione,$sezione,$tipoUtente){
	
    if($tipoUtente==0)$tipoUtente=5;
    if($tipoUtente>5)$tipoUtente=5;
    $dataFi=date('Y-m-d H:i:s');
	$aaFi=date('Y',strtotime($dataFi));
    $mmFi=date('m',strtotime($dataFi));
	$ddFi=date('d',strtotime($dataFi));
    $hhFi=date('H',strtotime($dataFi));
		$dataInRecenti=date('Y-m-d 00:00:00', time() - (3600 * 216));
        $aaIn=date('Y',strtotime($dataInRecenti));
        $mmIn=date('m',strtotime($dataInRecenti));
        $ddIn=date('d',strtotime($dataInRecenti));
        $hhIn=date('H',strtotime($dataInRecenti));
	echo "<br/>";
//----------------------------------------
	foreach ($stazioni as $id) {
	    $query="select nome from sito.stazioni where id=$id order by id";
	    $result = mysql_query($query,$linkLocale);
	    while ($riga=mysql_fetch_array($result)){
		$stazioniAArray[$id]=$riga['nome'];
	    }
	}
	asort($stazioniAArray);
	//print_r($stazioniAArray);
	echo "<select name='stazioni[]' multiple size=13 required>";
	while($staz=current($stazioniAArray)){
	//echo key($staz);
	    echo "<option value=".key($stazioniAArray).">".$staz." (".key($stazioniAArray).") </option>";
	    next($stazioniAArray);
	}
	echo "</select>";
	
//------------------------------------------------	
	echo "<select name='sensori[]' multiple size=13 required>";
//	if($livello==1){
		$query="select codice,sensore,misura from sito.sensori where livello>=$tipoUtente  order by ordine";
    $result = mysql_query($query,$linkLocale);
	while ($riga=mysql_fetch_array($result)){
		echo "<option value=".$riga['codice'].">".$riga['sensore']." ".$riga['misura']."</option>";
	}
	echo "</select>";
	echo "<br/><br/>";
    echo "  <input type='radio' name='tipo' value='stg' checked>Giornalieri
	    <input type='radio' name='tipo' value='st'>Orari";
    echo "<br/><br/>";
    echo "Data Inizio (Giorno Mese Anno Ora)";
    echo "<br/>";
    echo "<input type='text' name='gg_in' value=$ddIn size='3'   >
            <input type='text' name='mm_in' value=$mmIn size='3'>
            <input type='text' name='aa_in' value=$aaIn   size='3'> 
            
            <input type='text' name='hh_in' value=$hhIn size='3'   >";
    echo "<br/><br/>";
    echo "Data Fine (Giorno Mese Anno Ora)";
    echo "<br/>";
    echo " <input type='text' name='gg_fi' value=$ddFi size='3'  >
            <input type='text' name='mm_fi' value=$mmFi size='3'  > 
        <input type='text' name='aa_fi' value=$aaFi size='3'  >    
            <input type='text' name='hh_fi' value=$hhFi size='3' >";
    echo "<br/>";
    echo"<input type='submit' value='INVIA'>"; 
}
  
//================  funzione 'generica' per l'esrtazione dei dati===================================================================
function estraiDati($ids,$sensori,$dataIn,$dataFi,$tipo,$cf,$linkMeteo,$linkLocale){
//echo $ids;
if($id<=100)mysql_select_db('meteonesa',$linkMeteo);
if($id>4000 and $id<5000)mysql_select_db('davis',$linkMeteo);
if($id>=6000)mysql_select_db('davis',$linkMeteo);
if($id>=5000 and $id<6000)mysql_select_db('pessl',$linkMeteo);
//echo $db;

foreach($ids as $id){
    if(($id<=100) or $id==166)$db='meteonesa';
    if($id>4000 and $id<5000 or $id>=6000)$db='davis';
    if($id>=5000 and $id<6000)$db='pessl';
    $stazioni[]=$db.".".$tipo.$id;

}
$query="select ".$stazioni[0].".data";
$queryFrom=" from";
foreach($sensori as $sn){
	foreach ($stazioni as $st){
		$query.=", ";
		$query.="ifnull(".$st.".".$sn.",NULL) as ".$sn;
		$queryFrom.=" ".$st.",";
	}
}
$queryFrom=" from";
foreach ($stazioni as $st){
    $queryFrom.=" ".$st.",";
}
$query.=$queryFrom;
$query=rtrim($query,","); 
$query.=" where (".$stazioni[0].".data>='".$dataIn."' and ".$stazioni[0].".data <='".$dataFi."' )" ; 
if(count($stazioni)>1){
	for ($i=1;$i<count($stazioni);$i++){
		$query.=" and (".$stazioni[0].".data=".$stazioni[$i].".data)";
    }
}
$query.=" order by ".$stazioni[0].".data asc";
if ($cf=="CRRSFN68L31C372V"){
    echo $query;
}
$result = mysql_query($query,$linkMeteo);
$numFields=mysql_num_fields($result);
$numFields-=1;
$numRows=mysql_num_rows($result);
while($row = mysql_fetch_row($result)){
  $dataSet[] = $row; 
}
//----- memorizza query e n° dati
//print $query;
logAll($cf,$query,$numRows,$numFields,$linkLocale);
//----- ritorna 
return $dataSet;
}

//================   crea le intestazioni per le tabelle ???===================================================================
function intestaDati($stazioni, $sensori,$tipo,$linkLocale){
//$link1=connectSito();
$intestaTabella[]="Data";
foreach($sensori as $sn){
	$query="select sensore,misura,unit from sito.sensori where codice='$sn'";
	$result = mysql_query($query,$linkLocale);
	while ($riga=mysql_fetch_array($result)){
//		echo $riga[0];
//		echo $riga[1];
//		echo $riga[2];
		$unit=$riga[2];
		if(substr($unit,-1)=='C')$unit='&deg;C';
		$sensore=$riga[0]."<br/>".$riga[1]." [".$unit."]";
	//	$sensore=$riga[0]."<br/>".$riga[1]." [".$riga[2]."]";
	}
    foreach($stazioni as $st){
		$query="select nome from sito.stazioni where id=$st";
            $result = mysql_query($query,$linkLocale);
			while ($riga=mysql_fetch_array($result)){
                $staz=$riga['nome'];
            }
		$intestaTabella[]=$staz."<br/>".$sensore;
        }
}
//close_connection($link1);
return($intestaTabella);
}

//================   stampa a video la tabella HTML===================================================================
function printHtml($intestazioni,$dataSet){
	$tipoUtente=$_SESSION['tipoUtente'];
	$cf=$_SESSION['cf'];
	require_once "../inc/conn_lib.php";
	$link=connectSito();
	//$linkLocale=$_SESSION['linkLocale'];
	//print "$tipoUtente - $cf <br>";
	$fileHtml="html2png/".$cf.".html";
	$filePng="html2png/".$cf.".png";

	// il file PDF lo dovrei "tenere" e registrare il numero
	// num_sequenziale/anno e poi metto quest aindicazione in una tabella
	$query="select ifnull(max(numero),1) as numero,year(now()) as anno 
		from sito.montemeteopdf 
		where aa=year(now())";
	$result = mysql_query($query,$link)or die('Query failed: ' . mysql_error());
	$rows=array();
	$c=0;
	while($r=mysql_fetch_assoc($result)){
	    $numr=$r['numero'];
	    $annor=$r['anno'];
	}
//	print "num=$numr-$annor";


//	$fileHtmlFull="html2png/pdf/".$cf."-".$numr."-".$annor.".html";
//	$filePdf="html2png/pdf/".$cf."-".$numr."-".$annor.".pdf";
	$fileHtmlFull="html2png/pdf/report"."-".$numr."-".$annor.".html";
	$filePdf="html2png/pdf/report"."-".$numr."-".$annor.".pdf";
	
	$_SESSION['fileHtml']=$fileHtml;
	$_SESSION['filePng']=$filePng;
	$_SESSION['fileHtmlFull']=$fileHtmlFull;
	$_SESSION['filePdf']=$filePdf;
	
	if($tipoUtente=="3"){
	    // per tipo "Convenzione" NON visualizzo html
	    // ma png ...
	    // CREO il file HTML della SOLA tabella dati
	    $h=fopen($fileHtml,"w");
	    //if ($h==false){print "err";}else{print "ok";}
	    fwrite($h,"<table border=1><tr>");
	    foreach($intestazioni as $intesta){
		fwrite($h, "<th>$intesta</th>");
	    }
	    fwrite($h,"</tr>");
	    foreach($dataSet as $dataRow){
		fwrite($h,"<tr>");
		foreach($dataRow as $dataValue){
		    fwrite($h,"<td>$dataValue</td>");
		}
		fwrite($h,"</tr>");
	    }
	    fwrite($h,"</table>");
	    fclose($h);
	    
/*	    // CREO il file HTML "full" per pdf (logo+ scritte varie) + dati
	    //print $fileHtmlFull;
	    $h=fopen($fileHtmlFull,"w");
	    //if ($h==false){print "err";}else{print "ok";}
	    //
	    fwrite($h,"<center><img src=logo_fem_ufficiale.jpg width=300><br></center>");

	    $stringa="<center><br><br><font size=4>REPORT METEO </font><br></center>";
	    fwrite($h,$stringa);
	    
	    $stringa="<br><br>Numero report: $numr/$annor <br>";
	    fwrite($h,$stringa);

	    $inizio=$dataSet[0];
	    $conta=count($dataSet)-1;
	    $fine=$dataSet[$conta];
	    $stazione=substr($intestazioni[1],0,strpos($intestazioni[1],"<br"));
	    $stringa="<br>Si attesta che i dati rilevati dalla 
		stazione meteo di <b>".$stazione."</b> 
		dal ".$inizio[0]." al ".$fine[0]." sono i seguenti <br><br>";
	    fwrite($h,$stringa);

//	    $stringa="<br><br>".$fine[0]." <br>$conta<br>";
//	    fwrite($h,$stringa);

	    fwrite($h,"<table border=1><tr>");
	    
	    foreach($intestazioni as $intesta){
		fwrite($h, "<th>$intesta</th>");
	    }
	    fwrite($h,"</tr>");
	    foreach($dataSet as $dataRow){
		fwrite($h,"<tr>");
		foreach($dataRow as $dataValue){
		    fwrite($h,"<td>$dataValue</td>");
		}
		fwrite($h,"</tr>");
	    }
	    fwrite($h,"</table>");
	    fclose($h);
*/	    // CONVERTO ....
	    $r1=system("/var/www/html/meteo/html2png/converti.sh $fileHtml $filePng");
//	    $r1=system("/var/www/html/meteo/html2png/pdf/converti.sh $fileHtmlFull $filePdf");
	    print "<img src=$filePng>";
	} 
	if($tipoUtente<>"3"){
	    // utenti "standard" (esclusi convenzione)
	    echo "<table><tr>";
	    foreach($intestazioni as $intesta){
		echo "<th>".$intesta."</th>";
	    }
	    echo "</tr>";
	    foreach($dataSet as $dataRow){
		echo "<tr>";
		foreach($dataRow as $dataValue){
		    echo "<td>".$dataValue."</td>";
		}
		echo "</tr>";
	    }
	    echo "</table>";
	}
}

//================   funzione 'grezza per l'esportazion xls===================================================================
function printXls($intestazioni,$dataSet){
    $filename="sheet.xls";
   header ("Content-Type: application/vnd.ms-excel");
   header ("Content-Disposition: a; filename=$filename");
    echo "<table><tr>";
foreach($intestazioni as $intesta){
    echo "<td>".$intesta."</td>";
}
 foreach($dataSet as $dataRow){
     echo "<tr>";
     foreach($dataRow as $dataValue){
         echo "<td>".$dataValue."</td>";
    }
    echo "</tr>";
}
echo "</table>";
echo "<a href=".$filename.">File Xls";
}

//================  funzione pre l'esporazione xls usando la libreria PHPexcel===================================================================
function phpXls($intestazioni,$dataSet){
        // Create new PHPExcel object
    //echo date('H:i:s') . " Create new PHPExcel object\n";
    //print_r ($dataSet);
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
//echo date('H:i:s') . " Add some data\n";
//foglio attivo
    $objPHPExcel->setActiveSheetIndex(0);
    $letter="A";
    $number =1;
    $nomeXls=rand();
    $nomeXls.=".php";
    foreach($intestazioni as $intesta){
		$intesta=str_replace('&deg;','°',$intesta);
        $intesta=str_replace('<br/>',' ',$intesta);
        $cell=$letter.$number;
        $objPHPExcel->getActiveSheet()->SetCellValue($cell, $intesta);
        $letter++;
}
    foreach($dataSet as $dataRow){
        $letter="A";
        $number++;
        foreach($dataRow as $dataValue){
             $cell=$letter.$number;
            $objPHPExcel->getActiveSheet()->SetCellValue($cell, $dataValue);
           $letter++;
        }
    }
 $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$percorso="xls/";
$link="xls/";
$nomeXls=str_replace('.php', '.xls',$nomeXls);
$objWriter->save($percorso.$nomeXls);  

/*if($montemeteo>0){
    $nrecordx=calcolaDataset($dataSet);
    // aggiorno la tabella montemeteo...
    $query="update  sito.montemeteo set scaricato=scaricato+".$nrecordx." where cf='".$cf."';";
    echo $query;
//    $result = mysql_query($query,$linkLocale);
} else {
    print "mnte=$montemeteo<br>";
}
*/
return $link.$nomeXls;
}

//================   stampa il grafico lineare   -  singolo===================================================================
function graficoLineare ($stazioni,$sensori,$dataSet,$intestazioni,$linkLocale){
    $nValues=count($dataSet);		// conta il numero di array dati del dataset che è una rappresentazione matriciale della tabella
   $nt=round($nValues/10,0);		// numero tacche sul grafico
   for($i=0;$i<$nValues;$i++){		
        $dateArray[]=$dataSet[$i][0];		//estrae le date in posizione [0] e crea liarray delle date
    }    
    $nArrays=count($stazioni)*count($sensori);	// il numero di colonne di dati è dato dal numero di sensori per il nuemro di stazionie
    for ($i=1;$i<=$nArrays;$i++){						// per ogni "colonna" del dataset ...
        for($ii=0;$ii<count($dataSet);$ii++){		//... per tutta la lunghezza del dataset ...
            ${$i}[]=$dataSet[$ii][$i];						//...estrae i valori di una colonna e crea dinamicamente gli array da graficare
        } 
     }   

	$nf="xls/grafo_";		// crea il percorso e il nome file
	$i=1;
	// comincia il ciclo per la creazione dei grafici
	foreach($sensori as $sn){
		$nf.=$sn;
		$graph = new Graph(550,500);						// creazione grafico
		$graph->img->SetAntiAliasing(true);				// antialiasing
		$graph->SetScale("textlin");								// scala lineare
        $graph->xaxis->SetTickLabels($dateArray);	// date per la scala delle X
        $graph->xaxis->SetTextTickInterval($nt);		// numero si tacche visualizzate
	    $graph->img->SetMargin(50,50,100,150);	// margini interni sx-dx-up-down
        $graph->SetMarginColor('white');					// colore dei margini
        $graph->SetFrame(false);									// niente frame esterno
        $graph->SetBox(false);										// niente box esterno al grafico
		//$graph->SetShadow($aShowShadow=true,$aShadowWidth=5,$aShadowColor=array(102,102,102))
        $graph->xaxis->SetPos('min');						// posizione dell'asse
        $graph->xaxis->SetLabelAngle(90);				// inclinazione delle date
        $graph->xgrid->Show(true);							// visualizza la griglia
        $graph->xgrid->SetColor('lightblue');				// colore della griglia
        $graph->ygrid->SetColor('lightblue');
        $query="select sensore,misura,unit from sito.sensori where codice='$sn'";	// query per il titolo - parametro e unità di misura
		$result = mysql_query($query,$linkLocale);
		while ($riga=mysql_fetch_array($result)){
			$sensore=$riga[0]." ".$riga[1]." [".$riga[2]."]";
		}$graph->title->Set($sensore);										// setta il titolo
		$graph->legend->SetLayout(LEGEND_HOR);					// legenda orizzontale...
		$graph->legend->SetColumns(4);									// distribuita su 4 colonne
		$graph->legend->Pos(0.5,0.15,"center","bottom");		// posizione della legenda
        $colors=array('orange','red','blue','darkblue','lightgreen','darkgreen','teal','brown');		// array dei colori... da espandere
        $cc=0;																// contatore dei colori
        foreach($stazioni as $st){
			$nf=$nf."_".$st;
			$valuesArray=${$i};
			if($sn=='s17m45' or $sn=='s20m58'){		// se  pioggia o bagnatura uso barplot
				$lineplot=new BarPlot($valuesArray);
			}else{
				$lineplot=new LinePlot($valuesArray);
			}
			$query="select nome from sito.stazioni where id=$st";
			$result = mysql_query($query,$linkLocale);
			while ($riga=mysql_fetch_array($result)){
				$staz=$riga['nome'];
			}
			$lineplot->SetLegend("$staz");
			$lineplot->SetColor(4);
			$graph->Add($lineplot);
			$lineplot->SetColor($colors[$cc]);
			$cc++;
			if($sn=='s17m45' or $sn=='s20m58'){		// se necessario, calcolo i cumuli di pioggia o bagnatura
				$graph->SetYScale(0,'lin');							// aggiungo una seconda scala
				$cumulo[0]=0;												// inizializzo il cumulo
				$nValues=count($valuesArray);					// numero max di cicli
				for($ii=1;$ii<$nValues;$ii++){					
					$cumulo[$ii]=$cumulo[$ii-1]+$valuesArray[$ii];
				}
				$lineplot2=new LinePlot($cumulo);				// crea il grafico del cumulo,
				$lineplot2->SetLegend("$staz"."-cumulo");	// la legenda adatta
				$graph->AddY(0,$lineplot2);						// e lo aggiunge al grafico
				$lineplot2->SetColor($colors[$cc]);				// settando un nuovo colore
				$cc++;
			}
			$i++;
		}
    $graph->Stroke($nf);
    echo "<img src=".$nf.">";
    }
}// grafico

//================   funzione per la creazione di una legenda tabllare===================================================================
function dammiNomi($stazioni,$sensori,$linkLocale){
echo "<table>Legenda</th>";
foreach ($stazioni as $id) {
               $query="select nome from sito.stazioni where id=$id";
                $result = mysql_query($query,$linkLocale);
                 while ($riga=mysql_fetch_array($result)){
                            $nomiStazioni[$id]=$riga['nome'];
                            echo "<tr><td>".$id."</td><td>".$riga['nome']."</td></tr>";
                }
}
foreach ($sensori as $sn) {
    $query="select sensore,misura from sito.sensori where codice='$sn'";
    $result = mysql_query($query,$linkLocale);
    while ($riga=mysql_fetch_array($result)){
        $nomiSensori[$sn]=$riga[0]." ".$riga[1];
        echo "<tr><td>".$sn."</td><td>".$riga[0]." ".$riga[1]."</td></tr>";
    }
}
echo "</table>";
$nomi[0]=$nomiStazioni;
$nomi[1]=$nomiSensori;    
return $nomi;
}

//================   controlla che la data iniziale sia entro i limiti===================================================================
function dataCheck($dataIn,$scansione){
    //--- controllo che la data no sia troppo indietro
        $dataFi=date('Y-m-d H:i:s');
        $aaFi=date('Y',strtotime($dataFi));
        $mmFi=date('m',strtotime($dataFi));
        $ddFi=date('d',strtotime($dataFi));
        $hhFi=date('H',strtotime($dataFi));
    //$data_in=date('Y-m-d 00:00:00', time() - (3600 * 216));
        /*if($scansione=='ultimi'){
                $dataInRecenti=date('Y-m-d 00:00:00', time() - (3600 * 216));
                $aaIn=date('Y',strtotime($dataInRecenti));
                $mmIn=date('m',strtotime($dataInRecenti));
                $ddIn=date('d',strtotime($dataInRecenti));
                $hhIn=date('H',strtotime($dataInRecenti));
                $dataMinima=$aaIn."-".$mmIn."-".$ddIn." ".$hhIn.":00";
            }else{
                $aaIn=date('Y')-1;
                $mmIn="01";
                $ddIn="01";
                $hhIn="00";
                $dataMinima=$aaIn."-01-01 00:00:00";
		}*/
switch($scansione){
	case 'ultimi':
		$dataInRecenti=date('Y-m-d 00:00:00', time() - (3600 * 216));
        $aaIn=date('Y',strtotime($dataInRecenti));
        $mmIn=date('m',strtotime($dataInRecenti));
        $ddIn=date('d',strtotime($dataInRecenti));
        $hhIn=date('H',strtotime($dataInRecenti));
        $dataMinima=$aaIn."-".$mmIn."-".$ddIn." ".$hhIn.":00";
		break;
	case 'inps':
		$aaIn=date('Y');
		$dataMinima=$aaIn."-01-01 00:00:00";
		break;
	default:
		$aaIn=date('Y')-1;
                $mmIn="01";
                $ddIn="01";
                $hhIn="00";
                $dataMinima=$aaIn."-01-01 00:00:00";
}
//echo $dataIn;
//echo $dataMinima;

if(strtotime($dataIn)<strtotime($dataMinima)){
        echo "Data minima non rispettata!";
        return 0;
    }else{
        return 1;
    }
}

//================   funzione che visualizza messaggi specifici in base allo stato utente===================================================================
function stateMessages ($statoCrm){
//    echo "$statoCrm-";
    $pagina_info=$_SESSION['pagina_info'];

    switch ($statoCrm){
	case 0:
        	echo  "<div class=center><br/>Per visualizzare la pagina devi iscriverti al servizio o effettuare il login.";
        	echo "<br/><br/>Per ulteriori informazioni clicca 
        	    <a href=$pagina_info> QUI </a></div>";
            $do=0;
            break;
            case 2:
        	echo  "<div class=center><br/>Servizio non disponibile <br/>
        	    Sei in attesa di conferma dell'iscrizione.";
        	echo "<br/><br/>Per ulteriori informazioni clicca 
        	    <a href=$pagina_info> QUI </a></div>";
            $do=0;
            break;
            case 3:
        	echo  "<br/><div class=center>";
        	echo "Servizio non disponibile <br/><br/>";
        	echo "<br/><br/>Per ulteriori informazioni clicca 
        	    <a href=$pagina_info> QUI </a></div>";
            $do=0;
            break;
            case 4:
        	echo  "<div class=center><br/>Servizio non disponibile <br/>
        	    Devi selezionare le stazioni nel tuo profilo";
        	echo "<br/><br/>Per ulteriori informazioni clicca 
        	    <a href=$pagina_info> QUI </a></div>";
            $do=0;
            break;
            default:
            $do=1;
    }
    return $do;
}
//================   stampa il grafico  -  gestione dei grafi a barre e dei cumulati ===================================================================

function graficoLinea($stazioni,$sensori,$dataSet,$intestazioni,$linkLocale){
	$nValues=count($dataSet);		// conta il numero di array dati del dataset che è una rappresentazione matriciale della tabella
	//echo $nValues;
   $nt=round($nValues/10,0);		// numero tacche sul grafico
	if($nt<1)$nt=1;
//echo $nt;   
   for($i=0;$i<$nValues;$i++){		
        $dateArray[]=$dataSet[$i][0];		//estrae le date in posizione [0] e crea liarray delle date
    }    
    $nArrays=count($stazioni)*count($sensori);	// il numero di colonne di dati è dato dal numero di sensori per il nuemro di stazionie
    for ($i=1;$i<=$nArrays;$i++){						// per ogni "colonna" del dataset ...
        for($ii=0;$ii<count($dataSet);$ii++){		//... per tutta la lunghezza del dataset ...
            ${$i}[]=$dataSet[$ii][$i];						//...estrae i valori di una colonna e crea dinamicamente gli array da graficare
        } 
	}   
  	//echo $dateArray[0];
	$nf="img/grafo_";		// crea il percorso e il nome file
	$i=1;
	// comincia il ciclo per la creazione dei grafici
	foreach($sensori as $sn){
		$nf.=$sn;
		$graph = new Graph(650,500);						// creazione grafico
		$graph->img->SetAntiAliasing(true);				// antialiasing
		$graph->SetScale("textlin");								// scala lineare
        $graph->xaxis->SetTickLabels($dateArray);	// date per la scala delle X
        $graph->xaxis->SetTextTickInterval($nt);		// numero si tacche visualizzate
	    $graph->img->SetMargin(50,50,100,150);	// margini interni sx-dx-up-down
        $graph->SetMarginColor('white');					// colore dei margini
        $graph->SetFrame(false);									// niente frame esterno
        $graph->SetBox(false);										// niente box esterno al grafico
		//$graph->SetShadow($aShowShadow=true,$aShadowWidth=5,$aShadowColor=array(102,102,102))
        $graph->xaxis->SetPos('min');						// posizione dell'asse
        $graph->xaxis->SetLabelAngle(90);				// inclinazione delle date
        $graph->xgrid->Show(true);							// visualizza la griglia
        $graph->xgrid->SetColor('lightblue');				// colore della griglia
        $graph->ygrid->SetColor('lightblue');
        $query="select sensore,misura,unit from sito.sensori where codice='$sn'";	// query per il titolo - parametro e unità di misura
		$result = mysql_query($query,$linkLocale);
		while ($riga=mysql_fetch_array($result)){
			$sensore=$riga[0]." ".$riga[1]." [".$riga[2]."]";
			$misura="[$riga[2]]";
		}
		$graph->title->Set($sensore);										// setta il titolo
		$graph->yaxis->title->Set($misura);
		$graph->legend->SetLayout(LEGEND_HOR);					// legenda orizzontale...
		$graph->legend->SetColumns(4);									// distribuita su 4 colonne
		$graph->legend->Pos(0.5,0.15,"center","bottom");		// posizione della legenda
        //$colors=array('orange','red','blue','darkblue','lightgreen','darkgreen','teal','brown','violet','purple');		// array dei colori... da espandere
        $colors=array('orange','blue','red','green','teal','violet','peru');		// array dei colori... da espandere
        $colorsCum=array('orangered','navy','darkred','darkgreen','grey','purple','brown');		// array dei colori... da espandere
		$cc=0;																			// contatore dei colori
		if($sn=='s17m45' or $sn=='s16m45' or $sn=='s20m58' or $sn=='s6m26' or $sn=='et0_pm'){		// se  pioggia, bagnatura, insolazione o evapotraspirazione uso barplot
			unset ($gplots);
			foreach($stazioni as $st){
				$nf=$nf."_".$st;
				unset ($valuesArray);
				$valuesArray=${$i};
				$query="select nome from sito.stazioni where id=$st";
				$result = mysql_query($query,$linkLocale);
					while ($riga=mysql_fetch_array($result)){
				$staz=$riga['nome'];
				}
				$bplot=new BarPlot($valuesArray);				// creo il grafico a barre
				$bplot->SetLegend("$staz");							// setto la legenda
				$bplot->SetColor($colors[$cc]);				// colore del bordo
				$bplot->SetFillColor($colors[$cc]);					// colore di riempimento
				$gplots[]=$bplot;											// metto il plot "da parte" in un array per aggiungerlo successivamente
				$graph->yaxis->scale->SetGrace(50);
//				$graph->SetY2Scale('lin');							// aggiungo una seconda scala
				$cumulo[0]=0;												// inizializzo il cumulo
				$nValues=count($valuesArray);					// numero max di cicli
				for($ii=1;$ii<=$nValues;$ii++){					
					$cumulo[$ii]=$cumulo[$ii-1]+$valuesArray[$ii];		// calcolo il cumulato
				}
				if($sn=='s20m58'){
				$misura="[ore]";
				$fbc=0;
				$depo=$cumulo;
				foreach($depo as$val){
				    $cumulo[$fbc]=$val/60;
				    $fbc++;
				}
				}
				$graph->SetY2Scale('lin');							// aggiungo una seconda scala
				$graph->y2axis->title->Set($misura);
				$lineplot2=new LinePlot($cumulo);				// crea il grafico del cumulo,
				$lineplot2->SetLegend("$staz"."-cumulo");	// la legenda adatta
				$graph->AddY2($lineplot2);						// e lo aggiunge al grafico
				$lineplot2->SetColor($colorsCum[$cc]);				// settando un nuovo colore
				$cc++;
				$i++;
			}
			$lineplot=new GroupBarPlot($gplots);
			$lineplot->SetWidth(0.5);
			$graph->Add($lineplot);
			$graph->Stroke($nf);
			echo "<img src=".$nf.">";
		}else if($sn=='s6m57'){				// grafico per la radiazione con anche l'insolazione
				foreach($stazioni as $st){
					$nf=$nf."_".$st;
					$valuesArray=${$i};
					$query="select nome from sito.stazioni where id=$st";
					$result = mysql_query($query,$linkLocale);
					while ($riga=mysql_fetch_array($result)){
						$staz=$riga['nome'];
					}
					$lineplot=new linePlot($valuesArray);
					$lineplot->SetLegend("$staz");
					$lineplot->SetColor($colors[$cc]);
					$lineplots[]=$lineplot;
					//$bplot->SetColor($colors[$cc]);
				//$gplots[]=$bplot;
				//$cc++;
					$graph->SetYScale(0,'lin');							// aggiungo una seconda scala
					$graph->ynaxis[0]->scale->SetGrace(50);
				//  devo recuperare anche l'insolazione da sovrapporre alla radiazione
					if( strlen($dateArray[0])>10) {
						$query="select s6m26 from st$st where data>='".$dateArray[0]."' and data<='".end($dateArray)."' order by data asc";
					}else{
						$query="select s6m26 from stg$st where data>='".$dateArray[0]."' and data<='".end($dateArray)."' order by data asc";
					}
					//echo $query;
					$link=connectMeteo();
					$result = mysql_query($query,$link);
				//	$numFields=mysql_num_fields($result);
					//echo mysql_num_rows($result);
					while($row = mysql_fetch_row($result)){
						//echo $row[0];
						$insolazione[] = $row[0]; 
				}
				mysql_close($link);
				$bplot=new BarPlot($insolazione);				// crea il grafico del cumulo,
				$bplot->SetLegend("$staz"." insolazione");	// la legenda adatta
				$bplot->SetColor($colorsCum[$cc]);
				//$bplot->SetNoFill(); 
				$bplot->SetFillColor($colorsCum[$cc]);
				$gbplots[]=$bplot;
				$cc++;
				$i++;
		}
			$plot=new GroupBarPlot($gbplots);
			$graph->AddY(0,$plot);
			foreach($lineplots as $lineplot){
					$graph->Add($lineplot);
		}
			$graph->SetY2OrderBack('false');
			$graph->Stroke($nf);
			echo "<img src=".$nf.">";
		}else{
			foreach($stazioni as $st){
				$nf=$nf."_".$st;
				$valuesArray=${$i};
				$query="select nome from sito.stazioni where id=$st";
				$result = mysql_query($query,$linkLocale);
					while ($riga=mysql_fetch_array($result)){
				$staz=$riga['nome'];
				}
				$lineplot=new LinePlot($valuesArray);
				$lineplot->SetLegend("$staz");
				$lineplot->SetColor($colors[$cc]);
				$graph->Add($lineplot);
				$cc++;
				$i++;
			}
			$graph->Stroke($nf);
			echo "<img src=".$nf.">";
		}
	}
}// grafico2

//---------------------------- funzioni per  t5abelle pronte
//================  crea i menu per le tabelle pronte e  i modelli   gestisce anche la sola visualizzazione dell'elenco stazioni===================================================================
function creaMenuTabelle($stazioni,$linkLocale,$scansione,$sezione,$tipoUtente){
	if($tipoUtente==0)$tipoUtente=5;
    $dataFi=date('Y-m-d H:i:s');
	$aaFi=date('Y',strtotime($dataFi));
    $mmFi=date('m',strtotime($dataFi));
	$ddFi=date('d',strtotime($dataFi));
    $hhFi=date('H',strtotime($dataFi));
    //$data_in=date('Y-m-d 00:00:00', time() - (3600 * 216));
	echo "<br/>";
	foreach ($stazioni as $id) {
	    $query="select nome from sito.stazioni where id=$id order by id";
	    $result = mysql_query($query,$linkLocale);
	    while ($riga=mysql_fetch_array($result)){
		$stazioniAArray[$id]=$riga['nome'];
	    }
	}
	asort($stazioniAArray);
	//print_r($stazioniAArray);
	if($scansione=='pronte' or $scansione=='indici'){
		echo "<select name='stazioni[]'  size=10 required>";
	}else{
		echo "<select name='stazioni[]'  multiple size=10 required>";
	}
	while($staz=current($stazioniAArray)){
	//echo key($staz);
	    echo "<option value=".key($stazioniAArray).">".$staz." (".key($stazioniAArray).") </option>";
	    next($stazioniAArray);
	}
	echo "</select>";
	if($scansione=='pronte'){
//------------------------------------------------	
	echo "           <select name='tipo_report' size='10' required>
		  <option value='1' selected='selected' >1. Orario ultimi 10 giorni</option>
		  <option value='2'>2. Giornaliero anno in corso</option>
		  <option value='3'>3. Decadico anno in corso</option>
		  <option value='4'>4. Mensile anno in corso</option>
		  <option value='5'>5. Orario con selezione di data</option>
		  <option value='6'>6. Giornaliero con selezione di data</option>
		  <option value='7'>7. Decadico con selezione anno</option>
		  <option value='8'>8. Mensile con selezione anno</option>
		  <option value='9'>9. Annuale con selezione anno</option>
                </select>";
	echo "<br/><br/>";
	$dataInRecenti=date('Y-m-d 00:00:00', time() - (3600 * 216));
        $aaIn=date('Y',strtotime($dataInRecenti));
        $mmIn=date('m',strtotime($dataInRecenti));
        $ddIn=date('d',strtotime($dataInRecenti));
        $hhIn=date('H',strtotime($dataInRecenti));
    echo "Data Inizio (Giorno Mese Anno Ora)";
    echo "<br/>";
    echo "
            <input type='text' name='gg_in' value=$ddIn size='3'   >
            <input type='text' name='mm_in' value=$mmIn size='3'>
            <input type='text' name='aa_in' value=$aaIn   size='3'> 
            <input type='text' name='hh_in' value=$hhIn size='3'   >";
    echo "<br/><br/>";
    echo "Data Fine (Giorno Mese Anno Ora)";
    echo "<br/>";
    echo "<input type='text' name='gg_fi' value=$ddFi size='3'  >
            <input type='text' name='mm_fi' value=$mmFi size='3'  >
            <input type='text' name='aa_fi' value=$aaFi size='3'  > 
            
            <input type='text' name='hh_fi' value=$hhFi size='3' >";
    }
	echo "<br/><br/>";
	echo"<input type='submit' value='INVIA'>"; 
}


//==================estrazione tabella pronta===================================================================
function estraiTabellaPronta($stazione,$dataIn,$dataFi,$tipo,$cf,$linkMeteo,$linkLocale){
//    echo $dataIn;
	$query="select nome from sito.stazioni where id=$stazione[0]";
    $result = mysql_query($query,$linkLocale);
	while ($riga=mysql_fetch_array($result)){
        $nomeStazione=$riga['nome'];
	}
	
    if($stazione[0]<=100)$db='meteonesa';
    if($stazione[0]>4000 and $stazione[0]<5000 or $stazione[0]>=6000)$db='davis';
    if($stazione[0]>=5000 and $stazione[0]<6000)$db='pessl';
	
	
//	$db='meteonesa';
	//----- verifica del tipo di report
if ($tipo == 1){$rep=1;}
if ($tipo == 2){$rep=2;}
if ($tipo == 3){$rep=3;}
if ($tipo == 4){$rep=4;}
if ($tipo == 5){$rep=1;}	#era rep5
if ($tipo == 6){$rep=2;}	#era rep6
if ($tipo == 7){$rep=3;}	#era rep7
if ($tipo == 8){$rep=4;}	#era rep8
if ($tipo == 9){$rep=9;}
if ($tipo == 10){$rep=10;}
if ($tipo == 11){$rep=11;}
if ($tipo == 15){$rep=15;}
switch($rep){
    case 1:	
	if ($tipo == 1){print "Dati orari degli ultimi 10 giorni della stazione numero: $nomeStazione<hr>";}
	if ($tipo == 5){
    	    	print "Report orario dal $dataIn</b>  al $dataFi</b> della stazione $nomeStazione<br/><br/>";
	}  
	$query="select from_unixtime(unix_timestamp(now())-(10*24*3600));";
	$risultato=mysql_query($query,$linkMeteo);
	while($riga=mysql_fetch_row($risultato)){
	  $adesso=$riga[0];
	}
/*	  $query="set @cp:=0;set @cf:=0";
		mysql_query($query,$linkMeteo);
	$query="select date_format(data,'d-m-Y %H:%i'),
	  ifnull(s3m52,'--'),ifnull(s4m96,'--'),
	  ifnull(s17m45,'--'),
	  (@cp := @cp + ifnull(s17m45,0)) as cp,
	  ifnull(s20m58,'--'),
	  (@cf := @cf + ifnull(s20m58,0)) as cf,
	  ifnull(s1m50,ifnull(s13m50,'--')),
	  ifnull(s6m57,'--'),
	  ifnull(s37m52,'--'),ifnull(s38m52,'--')
	  from $db.st$stazione[0] ";*/
	$query="select date_format(data,'%d-%m-%Y %H:%i'),
	  ifnull(s3m52,'--'),ifnull(s4m96,'--'),
	  ifnull(s17m45,'--'),
	  @cp := @cp + ifnull(s17m45,0) as cp,
	  ifnull(s20m58,'--'),
	  @cf := @cf + round(ifnull(s20m58,0)/60,1) as cf,
	  ifnull(s1m50,ifnull(s13m50,'--')),
	  ifnull(s6m57,'--'),
	  ifnull(s37m52,'--'),ifnull(s38m52,'--')
	  from $db.st$stazione[0] join (select @cp:=0,@cf:=0) as c ";
	
	
	if ($tipo == 1){$query=$query." where data>'$adesso';";}
	  if ($tipo == 5){$query=$query." where data>='$dataIn' and data<='$dataFi';";}
	$intesta[]="DATA";
    $intesta[]="TEMP<br/> [&deg;C]";
    $intesta[]= "UMID<br/> [ %]";
    $intesta[]="PG<br/>[mm]";
    $intesta[]="PG_CUM<br/>[mm]";
    $intesta[]="FB<br/>[min]";
    $intesta[]="FB_CUM<br/>[ore]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]= "RAD<br/>[MJ/mq]";
    $intesta[]="T_ASC<br/>[&deg;C]";
    $intesta[]="T_BAG<br/>[&deg;C]";
	$meno=1;
	break;
	
	 case 2:
	$query="select year(now())";
	$risultato=mysql_query($query,$linkMeteo);
	while($riga=mysql_fetch_row($risultato)){
	  $adesso=$riga[0];
	}
	if ($tipo == 2){echo "Dati giornalieri anno in corso ($adesso) della stazione numero: $nomeStazione<hr>";}
	if ($tipo == 6){
				echo "Report giornaliero dal $dataIn</b>  al $dataFi</b> della stazione $nomeStazione<br/><br/>";
	}  
		$query="select date_format(data,\"%d-%m-%Y\"),
		n_ac,
		ifnull(s3m52,'--'),ifnull(s3m781,'--'),ifnull(s3m782,'--'),
		ifnull(s4m96,'--'),
		ifnull(s17m45,'--'),
		 @cp := @cp + ifnull(s17m45,0) as cp,
		ifnull(round(s20m58/60,1),'--'),
		ifnull(s2m51,ifnull(s13m51,'--')),
		ifnull(s1m50,ifnull(s13m50,'--')),ifnull(s1m64,ifnull(s13m64,'--')),
		ifnull(s6m57,'--'),
		ifnull(s5m54,'--'),
        et0_hg,
        et0_pm
		from $db.stg$stazione[0]  join (select @cp:=0) as c ";
		if ($tipo == 2){$query=$query." where data>'$adesso'";}
		if ($tipo == 6){$query=$query." where data>='$dataIn' and data<='$dataFi'";}
		$query=$query." order by data";
		
		$intesta[]="DATA";
		$intesta[]="N_AC";
    $intesta[]="TEMP <br/>[&deg;C]";
    $intesta[]="TEMP <br/>[&deg;C]";
    $intesta[]="TEMP<br/> [&deg;C]";
    $intesta[]= "UMID<br/> [ %]";
    $intesta[]="PG<br/>[mm]";
    $intesta[]="PG_CUM<br/>[mm]";
    $intesta[]="FB<br/>[ore]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]= "RAD<br/>[MJ/mq]";
    $intesta[]="PRES<br/>[mBar]";
    $intesta[]="ET0_H<br/>[mm]";
    $intesta[]="ET0_PM<br/>[mm]";
	$meno=2;	
    break;
	  case 3:
	$query="select year(now())";
	$risultato=mysql_query($query,$linkMeteo);
	while($riga=mysql_fetch_row($risultato)){
	  $adesso=$riga[0];
	}
	if ($tipo == 3){echo "Dati decadici anno in corso ($adesso) della stazione numero: $nomeStazione<hr>";}
	if ($tipo == 7){
		echo "Report decadico dal $dataIn</b>  al $dataFi</b> della stazione $nomeStazione<br/><br/>";
	}  

//		 @cp := @cp + ifnull(s17m45,0) as cp,   join (select @cp:=0) as c
		$query="select aa,mm,if(gg<=10,1,if(gg<=20,2,3)),
		round(avg(n_ac),1),
		round(avg(s3m52),1) 'TEM_MEDIA',
		round(avg(s3m781),1) 'TEM_MIN',
		round(avg(s3m782),1) 'TEM_MAX',
		round(avg(s4m96),0) 'UM_IGR',
		round(sum(s17m45),1) 'PG_MM',
		round(avg(ifnull(s2m51,s13m51)),1) 'VEN_DIR',
		round(avg(ifnull(s1m50,s13m50)),1) 'VEN_VEL',
		round(max(ifnull(s1m64,s13m64)),1) 'VEN_RAF',
		round(avg(s6m57),1) 'RAD_CAL',
        ifnull(round(avg(s5m54),1),'--') 'PAT'
		from $db.stg$stazione[0]";
		if ($tipo == 3){$query=$query." where data>'$adesso' ";}
		if ($tipo == 7){$query=$query." where data>='$dataIn' and data<='$dataFi' ";}
		$query=$query." group by aa,mm,if(gg<=10,1,if(gg<=20,2,3)) order by aa,mm,gg";
		$intesta[]="DATA";
		$intesta[]="MESE";
	$intesta[]="DECADE";
    $intesta[]="N_AC";
    $intesta[]="TEMP <br/>[&deg;C]";
    $intesta[]="TEMP MIN<br/>[&deg;C]";
    $intesta[]="TEMP  MAX<br/>[&deg;C]";
    $intesta[]= "UMID<br/> [ %]";
    $intesta[]="PG<br/>[mm]";
    //$intesta[]="PG_CUM<br/>[mm]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]= "RAD<br/>[MJ/mq]";
    $intesta[]="PRES<br/>[mBar]";
    $meno=4;
	break;
	case 4:
	$query="select year(now())";
	$risultato=mysql_query($query,$linkMeteo);
	while($riga=mysql_fetch_row($risultato)){
	  $adesso=$riga[0];
	}
	if ($tipo == 4){echo "Dati mensili anno in corso ($adesso) della stazione numero: $nomeStazione<hr>";}
    if ($tipo == 8){
        echo "Report mensile dal $dataIn</b>  al $dataFi</b> della stazione $nomeStazione<br/><br/>";
    }  
        $query="select aa,mm,
        round(avg(n_ac),1),
        round(avg(s3m52),1) 'TEM_MEDIA',
        round(avg(s3m781),1) 'TEM_MIN',
        round(avg(s3m782),1) 'TEM_MAX',
        round(avg(s4m96),0) 'UM_IGR',
        round(sum(s17m45),1) 'PG_MM',
        ifnull(round(sum(s20m58)/60,1),'--') 'FB',
        round(avg(ifnull(s2m51,s13m51)),1) 'VEN_DIR',
        round(avg(ifnull(s1m50,s13m50)),1) 'VEN_VEL',
        round(max(ifnull(s1m64,s13m64)),1) 'VEN_RAF',
        round(sum(s6m57),1) 'RAD_CAL',
        ifnull(round(avg(s5m54),1),'--') 'PAT'
        from $db.stg$stazione[0] ";
        if ($tipo == 4){$query=$query." where data>'$adesso' ";}
        if ($tipo == 8){$query=$query." where data>='$dataIn' and data<='$dataFi' ";}
        $query=$query." group by aa,mm order by aa,mm,gg";
		$intesta[]="DATA";
		$intesta[]="MESE";

    $intesta[]="N_AC";
    $intesta[]="TEMP<br/> [&deg;C]";
    $intesta[]="TEMP MIN <br/>[&deg;C]";
    $intesta[]="TEMP MAX<br/>[&deg;C]";
    $intesta[]= "UMID <br/>[ %]";
    $intesta[]="PG<br/>[mm]";
//$intesta[]="PG_CUM<br/>[mm]";
    $intesta[]="FB<br/>[ore]";
$intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]="VEN-VEL<br/>[m/s]";
    $intesta[]= "RAD<br/>[MJ/mq]";
    $intesta[]="PRES<br/>[mBar]";
	$meno=3;
	break;
	
	case 9:

    echo "Report annuale dal $dataIn</b>  al $dataFi</b> della stazione $nomeStazione[0]<br/><br/>";
        $query="select aa,
        round(avg(n_ac),1),
        round(avg(s3m52),1) 'TEM_MEDIA',
        round(avg(s3m781),1) 'TEM_MIN',
        round(avg(s3m782),1) 'TEM_MAX',
        round(avg(s4m96),0) 'UM_IGR',
        round(sum(s17m45),1) 'PG_MM',
        round(avg(s13m50),1) 'VEN_VEL',
        round(max(s13m64),1) 'VEN_RAF',
        round(avg(s6m57),1) 'RAD_CAL'
            from $db.stg$stazione[0] ";
    	$query=$query." where data>='$dataIn' and data<='$dataFi' ";
        $query=$query." group by aa order by aa,mm,gg";
    	$intesta[]="DATA";
		  $intesta[]="N_AC";
  
    $intesta[]="TEMP [&deg;C]";
  $intesta[]="TEMP [&deg;C]";
    $intesta[]="TEMP [&deg;C]";
  
  $intesta[]= "UMID [ %]";
    $intesta[]="PG [mm]";
    $intesta[]="VEN-VEL [m/s]";
  $intesta[]="VEN-VEL [m/s]";
      $intesta[]= "RAD [MJ/mq]";
    $meno=2;  
	break;
}// switch $rep	

$result = mysql_query($query,$linkMeteo);
$numFields=mysql_num_fields($result);
$numFields-=$meno;
$numRows=mysql_num_rows($result);
while($row = mysql_fetch_row($result)){
  $dataSet[] = $row; 
}	
		

//----- memorizza query e n° dati
logAll($cf,$query,$numRows,$numFields,$linkLocale);
//----- ritorna 
	$tabellaPronta[0]=$dataSet;
	$tabellaPronta[1]=$intesta;
	return $tabellaPronta;
	
}//estraiTabellaPronta


///================== indici Winkler e Huglin===================================================================
function indiciWH($cf,$id,$tipoUtente,$linkMeteo,$linkLocale){
	$stazione="stg".$id;
    $query="select nome_esteso,configurazione from gestione.stazioni_estesa where id=$id";
    $risultato=mysql_query($query,$linkMeteo);
    while ($righe=mysql_fetch_row($risultato)){
		$nomestazione=$righe[0];
		$configurazione=$righe[1];
    }
	$anno=date("Y",time());		// anno corrente
	if($tipoUtente==0){
		$anno_in=$anno;
		$intestaW[]='Giorno';
		$intestaW[]='Winkler<br/>'.$anno;
		$intestaH[]='Giorno';
		$intestaH[]='Huglin<br/>'.$anno;
	}else if ($tipoUtente<=2 and $tipoUtente>=1){
		$anno_in=$anno-5;
		$intestaW[]='Giorno';
		$intestaH[]='Giorno';
		for ($i=5;$i>=0;$i--){
			$a=$anno-$i;
			$intestaW[]='Winkler<br/>'.$a;
			$intestaH[]='Huglin<br/>'.$a;
		 }
	}else{
		$anno_in=$anno-1;
		$intestaW[]='Giorno';
		$intestaW[]='Winkler<br/>'.$anno_in;
		$intestaW[]='Winkler<br/>'.$anno;
		$intestaH[]='Giorno';
		$intestaH[]='Huglin<br/>'.$anno_in;
		$intestaH[]='Huglin<br/>'.$anno;
	}
	#$anno_in="2004";
	$id=0;
	
	$gc=0;
	//----------- date per legenda -------
	$dt_in=$anno_in."-04-01";
	$dt_fi=$anno_in."-10-31";
	$db='meteonesa';
	$query= "select data from $db.$stazione where data>='$dt_in' and data<='$dt_fi' order by data";
	$risultato=mysql_query($query,$linkMeteo);
	$i=0;
	while ($righe=mysql_fetch_row($risultato)){
		$datiW[$i][0]=substr($righe[0],5);    
		$datiH[$i][0]=substr($righe[0],5);    
		$i++;
}
	$n_date=count($data)-1;
	echo "Indici Winkler e Huglin <br/><br/>Stazione: $nomestazione<br/>";
	$i=1;
	for ($a=$anno_in;$a<=$anno;$a++){
		
			$gc++;
			$data_in=$a."-04-01";		// data di inzio periodo
			$data_end_w=$a."-10-31";	// datadi fine periodo per winkler
			$data_end_h=$a."-09-30";	// data di fine periodo per Huglins
//----------- Winkler  --------------
			//$query= "select if(s3m52>10,s3m52-10,0) from $db.$stazione where data>='$data_in' and data<='$data_end_w' order by data";
			$query= "select 
							@cp := @cp + if(s3m52>10,s3m52-10,0) as cp 
							from $db.$stazione  join (select @cp:=0) as c
							where data>='$data_in' and data<='$data_end_w' order by data";   //		if(s3m52>10,s3m52-10,0),
			$result=mysql_query($query,$linkMeteo);
			$numFields=mysql_num_fields($result);
			$numRows=mysql_num_rows($result);
			$ii=0;
			while ($righe=mysql_fetch_row($result)){
				$datiW[$ii][$i]=$righe[0];
				$ii++;
		}
		//----- memorizza query e n° dati
		logAll($cf,$query,$numRows,$numFields,$linkLocale);
// -------------- HUGLINS
			$query2= "select 
								@cp:=@cp + if(s3m52>10,round(1.04*((s3m52-10)+(s3m782-10))/2,1),0) as cp
								from $db.$stazione  join (select @cp:=0) as c
								where data>='$data_in' and data<='$data_end_h' order by data";   //								if(s3m52>10,round(1.04*((s3m52-10)+(s3m782-10))/2,1),0) ,
			$result2=mysql_query($query2,$linkMeteo);
			$numFields=mysql_num_fields($result2);
			$numRows=mysql_num_rows($result2);
			$ii=0;
			while ($righe=mysql_fetch_row($result2)){
				$datiH[$ii][$i]=$righe[0];
				$ii++;
			}
			$i++;
//----- memorizza query e n° dati
		logAll($cf,$query,$numRows,$numFields,$linkLocale);
}
	$dataSet[0]=$intestaW;
	$dataSet[1]=$datiW;
	$dataSet[2]=$intestaH;
	$dataSet[3]=$datiH;
	return $dataSet;
}// funciont indiciWH

//================================  modello per il calcolo del modello IPI, porting della vecchia  versione===================================
function buggiani($ids,$anno,$cf,$linkMeteo,$linkLocale){
	//print_r($ids);

		mysql_select_db('meteo_nt',$linkMeteo);
	$intesta[0]="Data";
	foreach($ids as $id){
		$stazioni[]="buggiani".$id;
		$query="select nome from sito.stazioni where id=$id";
		$result=mysql_query($query,$linkLocale);
        while ($riga=mysql_fetch_array($result)){
                $intesta[]=$riga['nome'];
            }
	}
	$query="select $stazioni[0].data ";
	foreach ($stazioni as $stazione){
		$query.=",$stazione.irc";
	}
	$query.=" from ";
	foreach ($stazioni as $stazione){
		$query.="$stazione,";
	}
	$query=rtrim($query,',');
	$query.=" where year($stazioni[0].data)=$anno  ";
	foreach ($stazioni as $stazione){
		$query.="and $stazioni[0].data = $stazione.data ";
	}
	$query .=" order by $stazioni[0].data asc";
	$result = mysql_query($query,$linkMeteo);
	$numFields=mysql_num_fields($result);
	$numFields-=1;
	$numRows=mysql_num_rows($result);
	//----- memorizza query e n° dati
		logAll($cf,$query,$numRows,$numFields,$linkLocale);
	while($row = mysql_fetch_row($result)){
	$dati[] = $row; 
	}
	if ($numRows==0){
		echo "Il modello funziona a partire dal 1 marzo.";
	}else{
	$dataSet[0]=$intesta;
	$dataSet[1]=$dati;
	return $dataSet;
	}
}// function buggiani

?>

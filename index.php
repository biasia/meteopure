<!doctype html>
<html lang="it">
<head>
    <?php include('head.html');?>
</head>

<body>
<div id="layout">
<!--  sezione menu --->
    <?php include('side-menu.html')	?>
    <!--   sezione principale-->
    <div id="main">
    <!-- sez header-->        
    <?php include('main-header.html')	?>
    <!-- sez content-->        
    <?php include('main-content.html')	?>
    </div><!-- div main-->
</div><!-- div layout-->
<!-- script per menu responsivo-->
<script src="js/ui.js"></script>
</body>
</html>
